package com.chewie.stopwatch;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    TextView textView;
    Button start, pause, restart, simpan;
    long MillisecondTime, StartTIme, TimeBuff, UpdateTime = 0L;
    Handler handler;
    int Seconds, minutes, Milliseconds;
    ListView listView;
    String[] ListElements = new String[]{};
    List<String> ListElementsArrayList ;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.textView);
        start = findViewById(R.id.button);
        pause = findViewById(R.id.button2);
        restart = findViewById(R.id.button3);
        simpan = findViewById(R.id.button4);
        listView = findViewById(R.id.listView);

        handler = new Handler();
        ListElementsArrayList = new ArrayList<String>(Arrays.asList(ListElements));
        adapter = new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_list_item_1,ListElementsArrayList);

        listView.setAdapter(adapter);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StartTIme = SystemClock.uptimeMillis();
                handler.postDelayed(runnable, 0 );

                restart.setEnabled(false);
            }
        });
        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimeBuff += MillisecondTime;
                handler.removeCallbacks(runnable);

                restart.setEnabled(true);
            }
        });
        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MillisecondTime = 0L;
                StartTIme = 0L;
                TimeBuff = 0L;
                UpdateTime = 0L;
                Seconds = 0;
                minutes = 0;
                Milliseconds = 0;

                textView.setText("00:00:00");

                ListElementsArrayList.clear();
                adapter.notifyDataSetChanged();

            }
        });
        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ListElementsArrayList.add(textView.getText().toString());

                adapter.notifyDataSetChanged();
            }
        });
    }
    public Runnable runnable = new Runnable() {
        @Override
        public void run() {
            MillisecondTime = SystemClock.uptimeMillis() - StartTIme;
            UpdateTime = TimeBuff + MillisecondTime;
            Seconds = (int) (UpdateTime / 1000);
            minutes = Seconds / 60;
            Seconds = Seconds % 60;
            Milliseconds = (int) (UpdateTime % 1000);
            textView.setText("" + minutes + ":"
                            + String.format("%02d", Seconds) + ":"
                            + String.format("%03d", Milliseconds)
            );
            handler.postDelayed(this, 0);


        }
    };

}
